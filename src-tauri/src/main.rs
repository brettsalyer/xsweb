use std::sync::{Arc, Mutex};
use std::thread;
use std::time::Duration;
use chrono::{DateTime, Local};
use tauri::State;
use xeta::xon::{XON, XonType};

const SECONDS_PER_DAY: u64 = 86400;
const CREATION_FEE: f64 = 15.00;
const XETA_FOR_XON: i64 = 10;

struct Simulation {
    previous_time: DateTime<Local>,
    current_time: DateTime<Local>,
    xon_count: u64,
    xons: Vec<XON>,
    xeta_balance: f64,
    usdc_balance: f64,
    bank_balance: f64,
    unclaimed_reward_balance: f64,
    xeta_value: f64
}

impl Simulation {
    pub fn get_xeta_produced(&mut self, time_delta: i64) -> f64 {
        let mut total_unclaimed = 0.0;

        // Loop over all xons and determine how much it should produce in time_delta.
        // Keep track of the total balance and return the new balanec since the time delta.
        for xon in self.xons.iter_mut() {
            xon.add_rewards(time_delta as f64 * (XON::get_emission_rate(xon.get_type()) / SECONDS_PER_DAY as f64));
            total_unclaimed += xon.get_unclaimed_rewards();

        }
        return total_unclaimed;
    }

    pub fn add_xon(&mut self, xon: XON) {
        self.xons.push(xon);
    }
}

fn main() {
    // Define our Simulation struct
    let sim = Arc::new(Mutex::new(Simulation {
        previous_time: Local::now(),
        current_time: Local::now(),
        xon_count: 0,
        xons: Vec::new(),
        xeta_balance: 0.0,
        usdc_balance: 0.0,
        bank_balance: 0.0,
        unclaimed_reward_balance: 0.0,
        xeta_value: 61.0
    }));

    let sim_thread = sim.clone();
    thread::spawn(move || {
        loop {
            let mut data = sim_thread.lock().unwrap();

            let new_time = data.current_time + chrono::Duration::seconds(1);
            let old_time = data.previous_time;

            // Update the time
            let time_delta = (data.current_time - old_time).num_seconds();

            data.previous_time = data.current_time;
            data.current_time = new_time;

            // The amount of unclaimed xeta should be increased by the time delta 
            let updated_unclaimed_balance = data.get_xeta_produced(time_delta);
            data.unclaimed_reward_balance = updated_unclaimed_balance;         

            drop(data);
            thread::sleep(Duration::from_secs(1));
        }
    });

    tauri::Builder::default()
        .manage(sim)
        .invoke_handler(tauri::generate_handler![
            get_current_time_handler,
            get_xeta_balance,
            get_unclaimed_rewards_balance,
            get_usdc_balance,
            get_bank_balance,
            get_xeta_value,
            get_xon_count,
            advance_time,
            deposit_usdc,
            purchase_xeta,
            create_xon
        ])
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
}

#[tauri::command]
fn get_current_time_handler(state: State<Arc<Mutex<Simulation>>>) -> String {
    let data = state.lock().unwrap();
    let current_time = &data.current_time;
    current_time.format("%d/%m/%Y %H:%M:%S").to_string()
}

#[tauri::command]
fn get_xeta_balance(state: State<Arc<Mutex<Simulation>>>) -> f64 {
    return state.lock().unwrap().xeta_balance;
}

#[tauri::command]
fn get_unclaimed_rewards_balance(state: State<Arc<Mutex<Simulation>>>) -> f64 {
    return state.lock().unwrap().unclaimed_reward_balance
}

#[tauri::command]
fn get_usdc_balance(state: State<Arc<Mutex<Simulation>>>) -> f64 {
    return state.lock().unwrap().usdc_balance;
}

#[tauri::command]
fn get_bank_balance(state: State<Arc<Mutex<Simulation>>>) -> f64 {
    return state.lock().unwrap().bank_balance;
}

#[tauri::command]
fn get_xeta_value(state: State<Arc<Mutex<Simulation>>>) -> f64 {
    return state.lock().unwrap().xeta_value;
}

#[tauri::command]
fn get_xon_count(state: State<Arc<Mutex<Simulation>>>) -> i64 {
    return state.lock().unwrap().xons.len() as i64;
}

#[tauri::command(rename_all = "snake_case")]
fn advance_time(state: State<Arc<Mutex<Simulation>>>, time_days: i64) {
    let mut sim = state.lock().unwrap();
    sim.current_time += chrono::Duration::days(time_days);
}

#[tauri::command(rename_all = "snake_case")]
fn deposit_usdc(state: State<Arc<Mutex<Simulation>>>, amount: f64) {
    let mut sim = state.lock().unwrap();
    sim.usdc_balance += amount;
}

#[tauri::command(rename_all = "snake_case")]
fn purchase_xeta(state: State<Arc<Mutex<Simulation>>>, amount: f64) {
    let mut sim = state.lock().unwrap();

    let usdc_balance = sim.usdc_balance;
    let xeta_value = sim.xeta_value;
    let usdc_required = xeta_value * amount;

    if usdc_balance >= usdc_required {
        sim.usdc_balance -= usdc_required;
        sim.xeta_balance += amount;
    } else {
        println!("Insufficient USDC");
    }
}

#[tauri::command(rename_all = "snake_case")]
fn create_xon(state: State<Arc<Mutex<Simulation>>>, amount: i64) {
    let mut sim = state.lock().unwrap();

    if sim.usdc_balance >= amount as f64 * CREATION_FEE {
        for _xon in 0..amount {
            let new_xon = XON::new(
                sim.xons.len() as i32 + 1,
                XonType::Standard,
                sim.current_time
            );

            sim.usdc_balance -= CREATION_FEE;
            sim.xeta_balance = sim.xeta_balance - XETA_FOR_XON as f64;
            sim.add_xon(new_xon);
        }
    }
    else {
        println!("Not enough USDC to cover the creation fee.");
    }
}