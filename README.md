# XSWEB
![alt text](docs/logo.png)  
  
## About
XSWEB (Xeta Simulator) is a simulation tool that can be used to simulate different outcomes based on certain actions taken in  
the Xeta Capital ecosystem of financial services.  
  
With the growing list of options being made available to grow your wealth, it is sometimes a dautning task to try and work out the math yourself, or to create large, complicated spreadsheets.  


This tools aims to let you test out your strategies before committing to them.  

You can deposit (simulated) USDC into the simulation at any point. You can use that USDC to pay for fees, purchase X3TA, create XONs, etc.  


It works just like it would on the Xeta Capital dapp. This sim tool will keep track of all your balances while you try out different strategies.  

This project is in it's infancy and does not yet have all of these features. If there is a feature you want, please make a feature request! If you would like to contribute, 
please reach out to me in the Xeta Capital Discord (@brettsalyer).

# Running XSWEB
XSWEB is currently not deployed or hosted on any web platform - yet! Eventually it will be. But for now, if you want to test out what I have currently, you can grab the latest development build from this Gitlab repository.

# Contributing
## Recommended IDE Setup

- [VS Code](https://code.visualstudio.com/) + [Tauri](https://marketplace.visualstudio.com/items?itemName=tauri-apps.tauri-vscode) + [rust-analyzer](https://marketplace.visualstudio.com/items?itemName=rust-lang.rust-analyzer)

