use chrono::{DateTime, Local};

pub mod xon {
    #[derive(Copy, Clone)]
    pub enum XonType {
        Standard,
        Friend,
        Xpre,
    }

    pub struct XON {
        id: i32,
        creation_date:  chrono::DateTime<chrono::Local>,
        expiration_date: chrono::DateTime<chrono::Local>,
        xon_type: XonType,
        emission_rate: f64,
        unclaimed_rewards: f64
    }

    impl XON {
         pub fn new(id: i32, xon_type: XonType, date_created: chrono::DateTime<chrono::Local>) -> Self {
           return XON {
            id: id,
            creation_date: date_created,
            expiration_date: date_created + chrono::Duration::days(365),
            xon_type: xon_type.clone(),
            emission_rate: Self::get_emission_rate(xon_type),
            unclaimed_rewards: 0.0
           } 
        }

        pub fn get_emission_rate(xon_type: XonType) -> f64 {
            match xon_type {
                XonType::Standard => return 0.1,
                XonType::Friend => return 0.075,
                XonType::Xpre => return 0.125
            }
        }

        pub fn add_rewards(&mut self, rewards: f64) {
            self.unclaimed_rewards += rewards;
        }

        pub fn get_type(&self) -> XonType{
            return self.xon_type;
        }

        pub fn get_unclaimed_rewards(&self) -> f64 {
            return self.unclaimed_rewards;
        }
    }
}
