const { invoke } = window.__TAURI__.tauri;

const DAY = 1
const WEEK = 7

let xeta_balance;

async function advanceTimeFromInput() {
  const textBox = document.getElementById("time_input");
  const days = parseInt(textBox.value);
  await invoke('advance_time', {time_days: days}).then((result) => {
  }).catch((error) => {
    console.error(error);
  });
}

async function advanceTime(days=1) {
  const textBox = document.getElementById("time_input");
  await invoke('advance_time', {time_days: days}).then((result) => {
    console.log(result);
  }).catch((error) => {
    console.error(error);
  });
}

async function depositUSDC() {
  // Get the amount to deposit from the input box
  const amount = parseFloat(document.getElementById("usdc_deposit_input").value);

  await invoke('deposit_usdc', {amount: amount})
    .then((response) => {
    })
    .catch((error) => {
      console.log(error);
    })
}

async function purchaseXeta() {
  // Get the amount to purchase from the input box
  const amount = parseFloat(document.getElementById("xeta_purchase_input").value);

  await invoke('purchase_xeta', {amount: amount})
    .then((response) => {
    })
    .catch((error) => {
      console.log(error);
    })
}

async function createXON() {
  const amount = parseInt(document.getElementById("xons_to_create").value);

  await invoke('create_xon', {amount: amount})
  .then((response) => {
  })
  .catch((error) => {
    console.log(error);
  })
}

async function fetchUnclaimedRewards() {
  await invoke('get_unclaimed_rewards_balance')
    .then((response) => {
      window.unclaimed_rewards.innerHTML = parseFloat(response).toLocaleString(undefined, {minimumFractionDigits: 5});
    })
    .catch((error) => {
      console.log(error);
    })
}
setInterval(fetchUnclaimedRewards, 1000);

async function fetchUSDCBalance() {
  await invoke('get_usdc_balance')
    .then((response) => {
      window.usdc_balance.innerHTML = parseFloat(response).toLocaleString(undefined, {minimumFractionDigits: 2});
    })
    .catch((error) => {
      console.log(error);
    })
}
setInterval(fetchUSDCBalance, 1000);

async function fetchXetaBalance() {
  await invoke('get_xeta_balance')
  .then((response) => {
      xeta_balance = parseFloat(response).toLocaleString(undefined, {minimumFractionDigits: 3});;

      window.xeta_balance.innerHTML = xeta_balance;
      window.xeta_balance_status.innerHTML = xeta_balance;
    })
    .catch((error) => {
      console.log(error);
    })
}

setInterval(fetchXetaBalance, 1000);

async function fetchXonCount() {
  await invoke('get_xon_count')
    .then((response) => {
      window.xon_count.innerHTML = response
    })
    .catch((error) => {
      console.log(error);
    })
}

setInterval(fetchXonCount, 1000);

async function fetchTime() {
  await invoke('get_current_time_handler')
    .then((response) => {
      window.time.innerHTML = response
    })
    .catch((error) => {
      console.log(error);
    })
}
setInterval(fetchTime, 1000);


let advance_button;
let advance_day_button;
let advance_week_button;
let add_usdc_button;
let usdc_deposit_button;
let usdc_popup;
let buy_xeta_button;
let xeta_purchase_popup;
let xeta_confirm_purchase_button;

let xon_slider;
let toggle_xon_button;
let create_xon_popup;
let create_xon_button;

window.addEventListener("DOMContentLoaded", () => {
  advance_button = document.getElementById("advance-button");
  advance_day_button = document.getElementById("advance-day-button");
  advance_week_button = document.getElementById("advance-week-button");

  add_usdc_button = document.getElementById("add_usdc_button");
  usdc_deposit_button = document.getElementById("usdc_deposit");
  usdc_popup = document.getElementById("usdc_menu");

  buy_xeta_button = document.getElementById("buy_xeta_button");
  xeta_confirm_purchase_button = document.getElementById("xeta_confirm_purchase_button");
  xeta_purchase_popup = document.getElementById("buy_xeta_menu");

  toggle_xon_button  = document.getElementById("toggle_xon_button");
  create_xon_popup = document.getElementById("create_xon_menu");
  xon_slider = document.getElementById("create_xon_input");
  create_xon_button = document.getElementById("create_xons_button")

  advance_button.addEventListener("click", () => advanceTimeFromInput());
  advance_day_button.addEventListener("click", () => advanceTime(DAY));
  advance_week_button.addEventListener("click", () => advanceTime(WEEK)); 

  add_usdc_button.addEventListener("click", () => {
    usdc_popup.style.visibility = usdc_popup.style.visibility == "visible" ? "hidden" : "visible";
  });

  usdc_deposit_button.addEventListener("click", () => {
    depositUSDC();
    usdc_popup.style.visibility = "hidden";

  });

  buy_xeta_button.addEventListener("click", () => {
    xeta_purchase_popup.style.visibility = xeta_purchase_popup.style.visibility == "visible" ? "hidden" : "visible";
  });

  xeta_confirm_purchase_button.addEventListener("click", () => {
    purchaseXeta();
    xeta_purchase_popup.style.visibility = "hidden";

  });

  toggle_xon_button.addEventListener("click", () => {
    create_xon_popup.style.visibility = create_xon_popup.style.visibility == "visible" ? "hidden" : "visible";
  });

  xon_slider.addEventListener("input",() => {
    let xons_to_create = document.getElementById("xons_to_create");

    let can_create = Math.floor(xeta_balance / 10);
    xons_to_create.innerHTML = Math.floor(xon_slider.value / 100 * can_create);
    xons_to_create.value = Math.floor(xon_slider.value / 100 * can_create);
  });

  create_xon_button.addEventListener("click", () => {
    createXON();
    create_xon_popup.style.visibility = "hidden";
  });
});